#!/bin/ksh
#
#===============================================================================================
#
#  ORACLE Trace File Management
#
#  Checks for new Tracefiles and alerts DBA via mail
#
#===============================================================================================
#
#  A simple script that checks for tracefiles in udump/bdump/cdump destinations (OFA!) and
#  send DBA a mail with full file-content so you can read it in the mail.
#
#  User traces are mailed once and zipped up immediate. 
#  Other trace are also mailed once but you keep a reminder on screen for 1 days
#  After 3 days of inactivity the renamed files are zipped.
#
#  If you're using that script - please feel free to send me some feedback or just
#  what you think about that programm.
#
#  Author : Alexander Venzke (Alexander.Venzke@gmx.net / Alexander.Venzke@dab.com)
#  Modified:  09-05-2007 Lau Daniels
#===============================================================================================
#
# Variablen
#
# your path env should include path to oraenv (/usr/local/bin or $ORACLE_HOME/bin)

dba=helpdesk@your.organistation.com							#change to your dba email address
FILENAME=/log-location/zipped-traces/RAC01-TRACES-`date '+%y%m%d'`.zip
DATE=date
zipfile=$ORACLE_HOME/bin/zip
ORACLE_HOME=$ORACLE_HOME                         #for diff userscript
INFOFILENAME=/log-location/zipped-traces/RAC01-TRACE-INFO-`date '+%y%m%d'`.txt

#===============================================================================================
#
# sendmail
#
#===============================================================================================

send_mail () {
        mailx -s "TraceFileCheck $info from host $(hostname) at $(date)" $dba < $1
        sleep 1
        rm -f $1

}

#===============================================================================================
#
# file_delete
# archive old files to /u02/backup
#
#===============================================================================================

file_check () {

        cd $1
		find . -name \*.tozip -exec $zipfile -m $FILENAME {} \;
        find . -name \*on-hold  -exec $zipfile -m $FILENAME {} \;
        find . -name \*.trm  -exec $zipfile -m $FILENAME {} \;
}


#===============================================================================================
#
# check_trace
# checking new tracefiles
#
#===============================================================================================

check_trace () {

        cd $1

        for tracefile in `ls -liatr *.trc 2>/dev/null | awk '{print $10}'`
        do
                info="nothing"
                echo "filename: ${tracefile}   .... working......" >> $INFOFILENAME
				#create a new file for mailing tracefile results
				echo "filename: ${tracefile}   - parsing only last 50 lines of trace - " > $INFOFILENAME.tmp
                cat ${tracefile} | grep "PARSING IN CURSOR" > /dev/null
                if [ $? = 0 ] ; then
                        #Usertracefile detected and not so important to us........
                                echo "user trace found. Mail to dba and zipped up" >> $INFOFILENAME.tmp
                                info="User-trace"
                                echo "User trace file detected: $1 - ${tracefile}" >> $INFOFILENAME.tmp
                                tail -n50 ${tracefile} >> $INFOFILENAME.tmp
                                mv ${tracefile} usertrace_${ORACLE_SID}_${datetime}.ut.tozip
				elif	cat ${tracefile} | grep -e "ORA-" -e "WARNING " > /dev/null; then
                        # ORA error or WARN found in tracefile
                                #Check if it's modified in the last 2 days if so do NOT rename it
                                thefile=`find . -mtime -2 -name ${tracefile} -print  2>/dev/null`
                                if [ -n "$thefile" ] ; then
										#file is not older than 2 days so no rename and send only if modfied in last 24 hours
                                        thefile=`find . -mtime -1 -name ${tracefile} -print  2>/dev/null`
                                        if [ -n "$thefile" ] ; then
												info="ORA-error"
                                                echo "Found ORA-error or WARNING in file: $1/${tracefile}" >> $INFOFILENAME.tmp
												echo "Found ORA-error or WARNING in file: $1/${tracefile}"
                                                ls -liatr ${tracefile} >> $INFOFILENAME.tmp
                                                tail -n50 ${tracefile} >> $INFOFILENAME.tmp                                                 
                                        else
                                                echo "REMINDER: oude ORA error or WARNING in file: $1/${tracefile}" >> $INFOFILENAME.tmp
                                        fi                                               
                                else
                                        mv ${tracefile} ${tracefile}_processed.${datetime}.error.tozip
								fi
					
				elif	cat ${tracefile} | grep -i " DEADLOCK DETECTED " > /dev/null; then
						# deadlock found in tracefile
											#Check if it's not modified in the last 2 days if not then rename
											thefile=`find . -mtime -2 -name ${tracefile} -print  2>/dev/null`
											if [ -n "$thefile" ] ; then
													#file is not older than 2 day so mail warning for all found files modified in the last 24 hours
													thefile=`find . -mtime -1 -name ${tracefile} -print  2>/dev/null`
													if [ -n "$thefile" ] ; then
															info="DEADLOCK"
															echo "Found deadlock in file: $1/${tracefile}" >> $INFOFILENAME.tmp
															echo "Found deadlock in file: $1/${tracefile}"
															ls -liatr ${tracefile} >> $INFOFILENAME.tmp
															tail -n50 ${tracefile} >> $INFOFILENAME.tmp
													else        
															echo "REMINDER: Found DEADLOCK in file: $1/${tracefile}" >> $INFOFILENAME.tmp
													fi
											else
													mv ${tracefile} ${tracefile}_processed.${datetime}.deadlock.tozip
											fi
				elif	cat ${tracefile} | grep -i " deadlock " > /dev/null; then
						# deadlock found but is it an Initialize deadlock. if so dont count.
											#Check if it's not modified in the last 2 days if not then rename
											thefile=`find . -mtime -2 -name ${tracefile} -print  2>/dev/null`
											if [ -n "$thefile" ] ; then
													#file is not older than 2 day so mail warning for all found files modified in the last 24 hours
													thefile=`find . -mtime -1 -name ${tracefile} -print  2>/dev/null`
													if [ -n "$thefile" ] ; then
															cat ${tracefile} | grep -i "Initialize deadlock" > /dev/null
															# deadlock found but is it an Initialize deadlock. if so dont count.
															if [ $? != 0 ] ; then
																			info="DEADLOCK"
																			echo "Found deadlock in file: $1/${tracefile}" >> $INFOFILENAME.tmp
																			ls -liatr ${tracefile} >> $INFOFILENAME.tmp
																			tail -n50 ${tracefile} >> $INFOFILENAME.tmp
															fi				
													else        
																			echo "REMINDER: Found DEADLOCK in file : $1/${tracefile}"	>> $INFOFILENAME.tmp	
													fi
											else
													mv ${tracefile} ${tracefile}_processed.${datetime}.deadlock.tozip
											fi
				else
                        # other error ..
                        #Check if it's not modified in the last 3 days, if not then rename
                                thefile=`find . -mtime -3 -name ${tracefile} -print  #2>/dev/null`
                                        if [ -n "$thefile" ] ; then                       
                                                #file is not older than 3 days so no rename and send only if modified in the last 24 hours
                                                thefile=`find . -mtime -1 -name ${tracefile} -print  2>/dev/null`
                                                if [ -n "$thefile" ] ; then
												#check now for know errors
														cat ${tracefile} | grep -i "java.io.IOException" > /dev/null
															if [ $? = 0 ] ; then
																info="known error"	
														    elif	cat ${tracefile} | grep -i "kjgcr_DeleteSO" > /dev/null; then	
																info="known error"
															elif	cat ${tracefile} | grep -i "SERVICE NAME:(rman_DATABASE)" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "Control autobackup written to DISK device" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "minact-scn master-status" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "minact-scn slave-status" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "Control file enqueue hold time tracking dump " > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "kjgcr_Main: KJGCR_ACTION " > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "rman@rac01.your.organistation.com" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "Warning: log write elapsed time" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "finished replaying gcs resources" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "End DRM(663)" > /dev/null; then	
																info="known error"			
															elif	cat ${tracefile} | grep -i "SYS$BACKGROUND" > /dev/null; then	
																info="known error"
															elif	cat ${tracefile} | grep -i "kgsksysstop: successful" > /dev/null; then	
																info="known error"
															elif	cat ${tracefile} | grep -i "SYS$USERS" > /dev/null; then	
																info="known error"
															#elif	cat ${tracefile} | grep -i "purge queue_table SYS.SCHEDULER_FILEWATCHER_QT" > /dev/null; then	
															#	info="known error"
															#elif	cat ${tracefile} | grep -i "purge queue_table SYS.SCHEDULER_FILEWATCHER_QT" > /dev/null; then	
															#	info="known error"																														
															else		
																info="Unkown-error"
																echo "Found unknown-error in file : $1/${tracefile}" >> $INFOFILENAME.tmp
																ls -liatr ${tracefile} >> $INFOFILENAME.tmp
																tail -n50 ${tracefile} >> $INFOFILENAME.tmp
															fi
                                                else
                                                        echo "REMINDER Found unknown error: $1  filename: ${tracefile}" >> $INFOFILENAME.tmp
                                                fi
                                        else
                                                mv ${tracefile} ${tracefile}_processed.${datetime}.unknownerror.tozip
                                        fi
                fi

                 if [ -a $INFOFILENAME.tmp ] ; then
					if [[ $info = 'nothing' || $info = 'known error' ]] ; then
						rm $INFOFILENAME.tmp
					else	
                        send_mail $INFOFILENAME.tmp
					fi	
                fi
        done

}

#===============================================================================================
#
# Main Procedure
#
#===============================================================================================

                cd $ORACLE_HOME  
                echo "PERFORMING TRACE FILE CHECKING ON $(hostname) at $(date)" >> $INFOFILENAME
		#find ./admin/DATABASE/udump -size -10 -exec $zipfile -m  $FILENAME {} \; 
            # processing trace files and create *.tozip files
                echo "checking DATABASE/bdump trace files" >> $INFOFILENAME
                        check_trace /u01/app/oracle/diag/rdbms/DATABASE/DATABASE1/trace

            # deleting old files (.tozip) file_check
                echo "checking $ORACLE_HOME/admin/DATABASE for old files" >> $INFOFILENAME
                        file_check /u01/app/oracle/diag/rdbms/DATABASE/DATABASE1/trace
        
	    # report diskspace usage
			#df -h | mailx -s "Diskspace usage from host $(hostname) at $(date)" $dba

echo "TRACE CHECKING COMPLETE  at $(date)" >> $INFOFILENAME

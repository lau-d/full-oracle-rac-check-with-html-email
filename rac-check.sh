#!/bin/ksh
#
#===============================================================================================
#
#  ORACLE General Status check of instances and services
#
#===============================================================================================
#
#  This script checks almost all the functions of your oracle environment and
#  send DBA a mail with normal indications and the found results.
#
#
#  Author : Lau Daniels - based on different snippets.
#  Created:   28-06-2006 Lau Daniels
#  Modified:  05-05-2007 Lau Daniels - added OS commands
#  Modified:  09-05-2007 Lau Daniels - added RAC awareness
#  Modified:  19-11-2012 Lau Daniels - multiple bug fixes
#  Modified:  02-10-2017 Lau Daniels - adjust values for new environment
#===============================================================================================
#
# Variablen
#
# path env should include path to oraenv (/usr/local/bin or $ORACLE_HOME/bin)


dba=helpdesk@your.organistation.com                                            #dba email address

ORACLE_HOME=$ORACLE_HOME                   #Set environment used for diff userscript 
ORACLE_BASE=/u01/app/oracle
ORACLE_UNQNAME=DATABASE
export ORACLE_HOME 
export ORACLE_BASE
export ORACLE_UNQNAME 
# export correct fid based on hostname
if [ "$(hostname)" = "rac01.your.organistation.com" ]; then
   ORACLE_SID=DATABASE1
else   
ORACLE_SID=DATABASE2
fi 
export ORACLE_SID
export PATH=$ORACLE_HOME/bin:$PATH

datetime=`date +'%Y%m%d%H%M%S'`
datetimetext=`date +'%Y-%m-%d-%H:%M:%S'`
no-go-flag=0
subject="ORACLE RAC is OK - "
start_time=$(date +%s)
sql_command=" "


#
#===============================================================================================
#
# sendmail
#
#===============================================================================================

#send email to dba email address
send_mail () {
        mailx -s "$subject, $(hostname) has started a RAC health in $date_time " $dba < $1
        sleep 1
       # rm -f $1 
}

#execute a command and check for string to excist
do_command_string () {
		newstring=`${command_string}`
		echo "$newstring" | grep "$compare_string" >/dev/null
		if [[ $? -eq "0" ]]
			then
				echo "<dt><pre><h5>$command_string  $sql_command</h5></pre></dt><dd><pre>$newstring</pre></dt>"	>> /log-location/$datetime.log
		else
				#check op ignore string
				echo "$newstring" | egrep "$ignore_string" >/dev/null
					if [[ $? -eq "0" ]]
						then
							echo "<h4><a name='$explain_string'> $explain_string</a> - <A href='#top'>back to top</A></h4>"	>> /log-location/$datetime.log
							echo "<dd><pre><h5><b style='color:#FF9090; background:#F7FEf8E'>THIS CHECK has some known issues  --> </b><a href='#$explain_string'>$explain_string</a></h5></pre><hr></dd>"	>> /log-location/$datetime.fail
							echo "<dt><pre><h5><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK has some known issues   --> </b>$command_string  $sql_command</h5></pre></dt><dd><pre>$newstring</pre></dd>"	>> /log-location/$datetime.log				
						else
							no-go-flag=1 
							echo "<h4><a name='$explain_string'> $explain_string</a> - <A href='#top'>back to top</A></h4>"	>> /log-location/$datetime.log
							echo "<b>corrective action to take:</b> $corrective_string" >> /log-location/$datetime.log
							echo "<dd><pre><h5><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b><a href='#$explain_string'>$explain_string</a></h5></pre><hr></dd>"	>> /log-location/$datetime.fail
							echo "<dt><pre><h5><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b>$command_string  $sql_command</h5></pre></dt><dd><pre>$newstring</pre></dd>"	>> /log-location/$datetime.log				
					fi
		fi			
ignore_string="nothing like this appears"	# reset ingnore string
}

#execute a command and check for string NOT to excist
do_command_no_string () {
		newstring=`${command_string}`
		echo "$newstring" | grep "$compare_string" >/dev/null
		if [[ $? -eq "1" ]]
		then
				echo "<dt><pre><h5>$command_string  $sql_command</h5></pre><dt><dd><pre>$newstring</pre></dt>"	>> /log-location/$datetime.log
		else
				#check op ignore string
				echo "$newstring" | egrep "$ignore_string" >/dev/null
					if [[ $? -eq "0" ]]
					then
							echo "<dt><pre><h5>$command_string  $sql_command</h5></pre></dt><dd><pre>$newstring</pre></dt>"	>> /log-location/$datetime.log
							echo "<dd><pre><h5><b style='color:#FF9090; background:#F7FEf8E'>THIS CHECK has some known issues  --> </b><a href='#$explain_string'>$explain_string</a></h5></pre><hr></dd>"	>> /log-location/$datetime.fail				
				else		
							no-go-flag=1
							echo "<h4><a name='$explain_string'> $explain_string</a> - <A href='#top'>back to top</A></h4>"	>> /log-location/$datetime.log
							echo "<b>corrective action to take:</b> $corrective_string" >> /log-location/$datetime.log				
							echo "<dd><pre><h5><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b><a href='#$explain_string'>$explain_string</a></h5></pre><hr></dd>"	>> /log-location/$datetime.fail
							echo "<dt><pre><h5><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b>$command_string  $sql_command</h5></pre></dt><dd><pre>$newstring</pre></dd>"	>> /log-location/$datetime.log				
					fi
		fi
ignore_string="nothing like this appears"	# reset ingnore string
}


#execute a SQL command 
do_sql() {
   $ORACLE_HOME/bin/sqlplus -S / as sysdba  <<SQLEND
   set linesize 2000
   set pagesize 2000
   set markup html on
       $sql_command
SQLEND
}



#===============================================================================================
#
# Main Procedure
#								
#===============================================================================================

# Make sure only oracle can run our script - at our system oracle user id = 1101
if [ "$(id -u)" != "1101" ]; then
   echo $EUID " - This script must be run as oracle. login as root, su - oracle and run again." 1>&2
   exit 1
fi
#echo "user is oracle so continue...." 
#make sure script is not running already
if [ -e /log-location/check.lock ]; then
   echo "File /log-location/check.lock exists"
   exit 1
else
	#create lock file
	echo $datetime > /log-location/check.lock
	echo "File /log-location/check.lock does not exists"
fi

#The following creates the start of the HTML content:
# message Starts
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" > /log-location/$datetime.fail
echo '<html><head><META HTTP-EQUIV="Pragma" CONTENT="no-cache"></head><body bgcolor="#FFFFCC"><div><Center><h5><A name="top">===== PERFORMING ALL OVER CHECK of ORACLE at \$datetimetext   =====</A></h5>' >> /log-location/$datetime.fail
echo '<br>Running in oracle: crontab -l (list)  of -e (edit)  on RAC01 at 07:00, 11,00 and 13:00 and on RAC02 at 10:00 and 16:00 </center><dl>' >> /log-location/$datetime.fail

echo subject: start oracle checks on $(hostname) > start.txt
echo PERFORMING ALL OVER CHECK OF ORACLE at $datetime  on $(hostname) runs for about 50 sec silently....... >> start.txt
/usr/sbin/sendmail helpdesk@your.organistation.com < /log-location/start.txt


#check if all 108 multipath disk are available
echo "<hr><dt><p><h3>Checking status multipath disks -- 108 disks</dt></h3></p>" 						>> /log-location/$datetime.log
command_string="ssh oracle@rac01 ls /dev/sd* | wc -l"
compare_string="108"
explain_string="108 multipath disk should be present"
corrective_string="log on at RAC01 of RAC02 and check multipath -l and inform Oracle Sysadmin"
do_command_string


#check dbconsole en agent
echo "<hr><dt><p><h3>Checking console status on RAC01</dt></h3></p>" 						>> /log-location/$datetime.log
command_string="ssh oracle@rac01 ORACLE_UNQNAME=DATABASE /log-location/console status"
compare_string="Oracle Enterprise Manager 11g is running\|Agent is Running and Ready"
explain_string="Status DbConsole EM"
corrective_string="log on RAC01 - cd /tools - start dbconsole with script: console start"
do_command_string

echo "<hr><dt><p><h3>Checking console status on RAC02</dt></h3></p>" 						>> /log-location/$datetime.log
command_string="ssh oracle@rac02 ORACLE_UNQNAME=DATABASE /log-location/console status"
compare_string="Oracle Enterprise Manager 11g is running\|Agent is Running and Ready"
explain_string="Status DbConsole EM"
corrective_string="log on RAC02 - cd /tools - start dbconsole with script: console start"
do_command_string

#check status instance
echo "<hr><dt><p><h3> checking instances</dt></h3></p>"					>> /log-location/$datetime.log
command_string="$ORACLE_HOME/bin/srvctl status instance -d DATABASE -i DATABASE1"
compare_string="Instance DATABASE1 is running on node rac01"
explain_string="Status instance on RAC01"
corrective_string="$ORACLE_HOME/bin/srvctl start instance -d DATABASE -i DATABASE1"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status instance -d DATABASE -i DATABASE2"
compare_string="Instance DATABASE2 is running on node rac02"
explain_string="Status instance on RAC02"
corrective_string="$ORACLE_HOME/bin/srvctl start instance -d DATABASE -i DATABASE2"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status service -d DATABASE"
compare_string="Service dnet_DATABASE is running on instance(s) DATABASE1,DATABASE2\|Service job_DATABASE  is running on instance(s) DATABASE1,DATABASE2\|Service owb_DATABASE  is running on instance(s) DATABASE1,DATABASE2\|Service rman_DATABASE  is running on instance(s) DATABASE1,DATABASE2"
explain_string="Status DATABASE Services"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status nodeapps"
compare_string="VIP rac01-vip is running on node: rac01\|VIP rac02-vip is enabled\|VIP rac02-vip is running on node: rac02\|Network is running on node: rac01\|Network is running on node: rac02\|GSD is disabled\|GSD is not running on node: rac01\|GSD is not running on node: rac02\|ONS daemon is running on node: rac01\|ONS daemon is running on node: rac02"
explain_string="Status VIP addresses"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status asm -a"
compare_string="ASM is running on rac02,rac01\|ASM is enabled"
explain_string="Status ASM"
corrective_string=""
do_command_string

echo "<hr><dt><p><h3> checking database	</dt></h3></p>"					>> /log-location/$datetime.log
command_string="$ORACLE_HOME/bin/srvctl status database -d DATABASE -v"
compare_string="Instance DATABASE1 is running on node rac01 with online services dnet_DATABASE,job_DATABASE,owb_DATABASE,rman_DATABASE. Instance status: Open\|Instance DATABASE2 is running on node rac02 with online services dnet_DATABASE,job_DATABASE,owb_DATABASE,rman_DATABASE. Instance status: Open"
explain_string="Status Database DATABASE"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status listener"
compare_string="Listener LISTENER is enabled\|Listener LISTENER is running on node(s): rac02,rac01"
explain_string="Status Listener"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status diskgroup -g DG_DATA"
compare_string="Disk Group DG_DATA is running on rac02,rac01"
explain_string="Status DiskGroup DG_DATA"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status diskgroup -g DG_FRA"
compare_string="Disk Group DG_FRA is running on rac02,rac01"
explain_string="Status DiskGroup DG_FRA"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status diskgroup -g CRS_VOTING"
compare_string="Disk Group CRS_VOTING is running on rac02,rac01"
explain_string="Status DiskGroup CRS_Voting"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status oc4j"
compare_string="OC4J is enabled\|OC4J is running on node rac02"
explain_string="Status OC4J"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status cvu"
compare_string="CVU is enabled and running on node rac02"
explain_string="Status CVU"
corrective_string="$ORACLE_HOME/bin/srvctl relocate cvu -n rac02   - we want this on rac02"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status scan -i 1"
compare_string="scan1 is running on node rac01"
explain_string="Status Scan1 addresses"
corrective_string="$ORACLE_HOME/bin/srvctl relocate scan -i 1"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status scan -i 2"
compare_string="scan2 is running on node rac01"
explain_string="Status Scan2 addresses"
corrective_string="$ORACLE_HOME/bin/srvctl relocate scan -i 2"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status scan -i 3"
compare_string="scan3 is running on node rac02"
explain_string="Status Scan3 addresses"
corrective_string="$ORACLE_HOME/bin/srvctl relocate scan -i 3"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status server -n RAC01 -a"
compare_string="Server state: ONLINE\|Generic ora.DATABASE ora.DATABASE_dnet_DATABASE ora.DATABASE_job_DATABASE ora.DATABASE_owb_DATABASE ora.DATABASE_rman_DATABASE Server state details:"
explain_string="Status Server RAC01"
corrective_string="$ORACLE_HOME/bin/srvctl/relocate scan -i 2 -n rac02  - we want scan 2 on rac02"
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status server -n RAC02 -a"
compare_string="Server state: ONLINE\|Generic ora.DATABASE ora.DATABASE_dnet_DATABASE ora.DATABASE_job_DATABASE ora.DATABASE_owb_DATABASE ora.DATABASE_rman_DATABASE Server state details:"
explain_string="Status Server RAC02"
corrective_string=""
do_command_string

command_string="$ORACLE_HOME/bin/srvctl status srvpool"
compare_string="Server pool name: Free\|Active servers count: 0\|Active servers count: 2"
explain_string="Status ServerPool"
corrective_string=""
do_command_string

echo "<hr><dt><p><h3> Checking Cluster</dt></h3></p>"						>> /log-location/$datetime.log
command_string="/u01/app/11.2.0/grid/bin/crsctl check cluster"
compare_string="Cluster Ready Services is online\|Cluster Synchronization Services is online\|Event Manager is online"
explain_string="Status Cluster"
corrective_string=""
do_command_string

command_string="/u01/app/11.2.0/grid/bin/olsnodes -n -i"
compare_string="rac01\|rac01-vip\|rac02\|rac02-vip"
explain_string="Status RAC Vips"
corrective_string=""
do_command_string

command_string="/u01/app/11.2.0/grid/bin/crsctl check ctss"
compare_string="Service is in Observer mode"
explain_string="Status RAC TimeService"
corrective_string=""
do_command_string

command_string="/u01/app/11.2.0/grid/bin/ocrcheck"
compare_string="Cluster registry integrity check succeeded"
explain_string="Status ORC"
corrective_string=""
do_command_string

command_string="/u01/app/11.2.0/grid/bin/crsctl query css votedisk"
compare_string="ONLINE\|24aeb0f0c5b64f69bf3033df460709a2\|CRS_VOTING"
explain_string="Status VotingDisk"
corrective_string=""
do_command_string				


echo "<hr><dt><p><h3>Checking various SQL statements</dt></h3></p>" 						>> /log-location/$datetime.log

#Check for unusual status of the instance database DATABASE1
sql_command="select status, DATABASE_STATUS, active_state from gv\$instance where INSTANCE_NAME='DATABASE1';"
compare_string="STARTED\|MOUNTED\|MIGRATE\|SUSPENDED\|RECOVERY\|QUIES\|ERROR\|ORA"
command_string="do_sql"
explain_string="SQL Status database DATABASE1"
corrective_string=""
do_command_no_string

#Check for unusual status of the instance database DATABASE2
sql_command="select status, DATABASE_STATUS, active_state from gv\$instance where INSTANCE_NAME='DATABASE2';"
compare_string="STARTED\|MOUNTED\|MIGRATE\|SUSPENDED\|RECOVERY\|QUIES\|ERROR\|ORA"
command_string="do_sql"
explain_string="SQL Status database DATABASE2"
corrective_string=""
do_command_no_string

#Check for Invalid objects in DBA-objects
sql_command="select * FROM DBA_OBJECTS WHERE STATUS = 'INVALID';"
compare_string="SYSTEM\|OWBSYS\|ERROR\|ORA"
ignore_string='"IMAGEBANK"'
command_string="do_sql"
explain_string="SQL Status database invalid objects DBA"
corrective_string=""
do_command_no_string

#Check for Invalid objects in user-objects
sql_command="select * from user_objects WHERE STATUS = 'INVALID';"
compare_string="INVALID\|ERROR\|ORA"
command_string="do_sql"
explain_string="SQL Status database invalid object User"
corrective_string=""
do_command_no_string

#Space Monitoring – To check % used space in a tabelspace
sql_command="select tablespace_name,used_percent from dba_tablespace_usage_metrics where used_percent > 80;"
compare_string="no rows selected"
command_string="do_sql"
explain_string="SQL Status tablespaces"
corrective_string=""
do_command_string

#Check for BAD/ BROKEN jobs.
sql_command="select JOB,BROKEN,WHAT from dba_jobs where broken ='Y';"
compare_string="no rows selected"
command_string="do_sql"
explain_string="SQL Status jobs"
corrective_string=""
do_command_string

#Frequency of redo/archive generation- This gives redo switch frequency if crosses 150 
#parameter archive_lag_target is set to 900 (15 minutes), so 4 switches per hour = 96 per day on a idle system. set check to 150 for high load.
#Increase the log file size so that Oracle switches at the recommended rate of one switch per 15 to 30 minutes.
sql_command="select INST_ID,trunc(first_time),count(*) total_logs from gV\$LOGHIST where trunc(first_time) > sysdate -10 group by INST_ID, trunc(first_time) having count(*) > 150 order by trunc(first_time);"
compare_string="no rows selected"
command_string="do_sql"
explain_string="SQL Status RedoLogs more than 150"
corrective_string="Increase the log file size so that Oracle switches at the recommended rate of one switch per 15 to 30 minutes."
do_command_string

#Check if the backup taken is up to date without failures.
#select trunc(START_TIME),END_TIME,INPUT_BYTES,STATUS,INPUT_TYPE from v$rman_backup_job_details where trunc(start_time) > sysdate -2;
sql_command="select trunc(START_TIME),END_TIME,INPUT_BYTES,STATUS,INPUT_TYPE from v\$rman_backup_job_details where trunc(start_time) > sysdate -2;"
compare_string="FAILED\|ERROR\|ORA\|WARNING"
command_string="do_sql"
explain_string="SQL Status Backup check for failed"
corrective_string=""
do_command_no_string

#Check if the backup has run.
#select trunc(START_TIME),END_TIME,INPUT_BYTES,STATUS,INPUT_TYPE from v$rman_backup_job_details where trunc(start_time) > sysdate -2;
sql_command="select trunc(START_TIME),END_TIME,INPUT_BYTES,STATUS,INPUT_TYPE from v\$rman_backup_job_details where trunc(start_time) > sysdate -2;"
compare_string="no rows selected"
command_string="do_sql"
explain_string="SQL Status Backup has not run when there are no rows selected"
corrective_string=""
do_command_no_string


# found on page http://www.rampant-books.com/art_oracle_v_sqlstats.htm
#SELECT disk_reads, executions, disk_reads/executions, address, sql_text FROM v$sqlarea WHERE disk_reads > 1 and executions > 5 and (disk_reads/executions) > 10000 ORDER BY disk_reads;
sql_command="SELECT disk_reads, executions, disk_reads/executions, address, sql_text FROM v\$sqlarea WHERE executions > 5 and (disk_reads/executions) > 100000 ORDER BY DISK_READS;"
compare_string="no rows selected"
ignore_string='"sys.dba|sys.dba_audit_session|WRM|sys.wri|web_guid"'
command_string="do_sql"
explain_string="Heavy disk reads"
corrective_string="find responsible sql: select sql_text from v$sqltext where address = 'xxxxxxxxx' order by piece;<br>
Heavy disk reads per statement execution usually means a lack of proper indexing, poor selection criteria, etc.."
do_command_string


#SELECT buffer_gets, executions, buffer_gets/executions, address, sql_text FROM v$sqlarea WHERE buffer_gets > 1 and executions > 5 and (buffer_gets/executions) > 1000000 ORDER BY buffer_gets;
sql_command="SELECT buffer_gets, executions, buffer_gets/executions, address, sql_text FROM v\$sqlarea WHERE executions > 5 and (buffer_gets/executions) > 10000000 ORDER BY buffer_gets;"
compare_string="no rows selected"
ignore_string='"sys.dba|sys.dba_audit_session|WRM|sys.wri"'
command_string="do_sql"
explain_string="Heavy buffer reads"
corrective_string="find responsible sql: select sql_text from v$sqltext where address = 'xxxxxxxxx' order by piece;<br>
Heavy buffer reads sometimes means indexes are being used when they shouldnt be."
do_command_string


#Check HM Checker  for findings about the checkers - via em console
#   select *  from v$hm_finding;
#1. Access the Database Home page.
#2. In the Related Links section, click Advisor Central.
#3. Click Checkers to view the Checkers subpage.
#4. Click the run name for the checker run that you want to view
sql_command="select *  from v\$hm_finding;"
compare_string="no rows selected"
command_string="do_sql"
explain_string="SQL Status checkers"
corrective_string="http://practical-tech.blogspot.com/2012/01/oracle-11g-health-checks.html"
do_command_string

#check for deadlocks
#SELECT value FROM v$sysstat WHERE class=4 AND name= 'enqueue deadlocks';
sql_command="SELECT value FROM v\$sysstat WHERE class=4 AND name= 'enqueue deadlocks';"
compare_string="0"
command_string="do_sql"
explain_string="enqueue deadlocks"
corrective_string="http://www.dbaref.com/latches-and-enqueues <br>
enqueue deadlocks is how many times a deadlock situation has occured <br>
(every time the client receives an ORA-60 and a trace file will be created). <br>
This value should be zero, else an investigationshould be made and the trace files should be checked."
do_command_string

#SELECT ROUND(((1-(SUM(DECODE(name,'physical reads', VALUE,0))/(SUM(DECODE(name, 'db block gets', VALUE,0))+(SUM(DECODE(name, 'consistent gets', VALUE, 0))))))*100))|| '%' "Buffer Cache Hit Ratio" FROM v$sysstat;
#--Cache Hit Ratio - 14-05-2012 - 99.93%
sql_command="SELECT ROUND(((1-(SUM(DECODE(name,'physical reads', VALUE,0))/(SUM(DECODE(name, 'db block gets', VALUE,0))+(SUM(DECODE(name, 'consistent gets', VALUE, 0))))))*100))|| '%' \"Buffer Cache Hit Ratio\" FROM v\$sysstat;"
compare_string="99\|100"
command_string="do_sql"
explain_string="Cache Hit Ratio"
corrective_string="The database buffer cache hit ratio should be greater than 95% on an OLTP system. <br>
Otherwise increase the size of DB_CACHE_SIZE within SGA_MAX_SIZE boundary."
do_command_string

#SELECT (SUM(GETS-GETMISSES))/SUM(GETS) "Dictionary Cache Hit Ratio" FROM V$ROWCACHE;
#-- Dictionary Cache Hit Ratio - 14-05-2012 - .999534207
sql_command="SELECT (SUM(GETS-GETMISSES))/SUM(GETS) \"Dictionary Cache Hit Ratio\" FROM V\$ROWCACHE;"
compare_string=".99"
command_string="do_sql"
explain_string="Dictionary Cache Hit Ratio"
corrective_string="The overall dictionary cache hit ratio should be greater than .99"
do_command_string


#-------------------------------------------------   END OF SQL COMMANDS -----------------------------------------------
sql_command=" "

# multiple os commands to check OS

echo "<hr><dt><p><h3> Checking OS</dt></h3></p>"						>> /log-location/$datetime.log
# Percentage of disk usage from which to issue alerts
ALERT_USAGE=82
# You can pass extra switches to 'df' command if you want. Here are those that might be useful:
# -h     = outputs the information about filesystem usage in human readable form
# -l     = performs check only on local filesystems (so ignores filesystems connected via network)
# --sync = performs sync operation which makes the output more up to date, mainly on SunOS systems
#          (for more information see 'df' man page)
DF_EXTRA_SWITCHES="-h -l"
		
explain_string="Diskspace on RAC01"
echo "<dt><pre><h5><a name='$explain_string'>$explain_string</a></h5></pre></dt>"	>> /log-location/$datetime.log

LC_ALL="C"  ssh oracle@rac01  df -P $DF_EXTRA_SWITCHES | grep -vE "(^Filesystem|cdrom)" \
                | awk '{ print $1 " " $5 " " $2 " " $4 }' | while read output
do
        usage=`echo $output | awk '{ print $2 }' | cut -d'%' -f1`
		part=`echo $output | awk '{ print $1 }'`
        total=`echo $output | awk '{ print $3 }'`
        left=`echo $output | awk '{ print $4 }'`
	if [ $usage -ge $ALERT_USAGE ]; then
			echo "<dd><pre><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b><a href='#$explain_string'>$explain_string</a></pre></dd><hr>" >> /log-location/$datetime.fail
			echo "<dd><pre><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b> $part usage <b>$usage%</b> (left $left out of $total) - <A href='#top'>back to top</A></pre></dd>" >> /log-location/$datetime.log
			no-go-flag=1
	else
			echo "<dd><pre>$part usage <b>$usage%</b> (left $left out of $total)</pre></dd>" >> /log-location/$datetime.log		
	fi
done

explain_string="Diskspace on RAC02"
echo "<dt><pre><h5><a name='$explain_string'>$explain_string</a></h5></pre></dt>"	>> /log-location/$datetime.log	
		
LC_ALL="C"  ssh oracle@rac02  df -P $DF_EXTRA_SWITCHES | grep -vE "(^Filesystem|cdrom)" \
                | awk '{ print $1 " " $5 " " $2 " " $4 }' | while read output
do
        usage=`echo $output | awk '{ print $2 }' | cut -d'%' -f1`
		part=`echo $output | awk '{ print $1 }'`
        total=`echo $output | awk '{ print $3 }'`
        left=`echo $output | awk '{ print $4 }'`
	if [ $usage -ge $ALERT_USAGE ]; then
			echo "<dd><pre><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b><a href='#$explain_string'>$explain_string</a></pre></dd><hr>" >> /log-location/$datetime.fail
			echo "<dd><pre><b style='color:#FF0000; background:#F7FE2E'>THIS CHECK FAILED  --> </b>$part usage <b>$usage%</b> (left $left out of $total) - <A href='#top'>back to top</A></pre></dd>" >> /log-location/$datetime.log
			no-go-flag=1
	else
			echo "<dd><pre>$part usage <b>$usage%</b> (left $left out of $total)</pre><dd>" >> /log-location/$datetime.log		
	fi
done

rm /log-location/check.lock >> /log-location/$datetime.log


#controleren van trace files op rac01
echo "<hr><dt><p><h3>Checking trace files on RAC01</dt></h3></p>" 						>> /log-location/$datetime.log
command_string="ssh oracle@rac01 /log-location/traces-rac01"
compare_string="Found ORA-error or WARNING\|Found deadlock in directory"
explain_string="Status Trace files RAC01"
corrective_string=""
do_command_no_string

#controleren van trace files op rac02
echo "<hr><dt><p><h3>Checking trace files on RAC02</dt></h3></p>"						>> /log-location/$datetime.log
command_string="ssh oracle@rac02 /log-location/traces-rac02"
compare_string="Found ORA-error or WARNING\|Found deadlock in directory"
explain_string="Status Trace files RAC02"
corrective_string=""
do_command_no_string

# ----------------------------------- end of checks -------------------------------------------

#find out how long we did run
finish_time=$(date +%s)
total_time="$((finish_time - start_time)) secs."

# let see what we have discovered and prepare email
if [ $no-go-flag -eq 1 ] 
	then 
		subject="ORACLE RAC has FAILED -"
	else
		echo "No problems found on RAC"	>> /log-location/$datetime.fail
fi 

echo "</dl></div><h5><center>================= END  of CHECK $(date) with total run time of $total_time ===================</center></h5></p></body></html>"		>> /log-location/$datetime.log


#Prepare email log file for sending as html
echo "To: $DBA" > /log-location/$datetime.mail
echo "Subject: $subject, $(hostname) has checked RAC health in $total_time" >> /log-location/$datetime.mail
echo "Content-Type: text/html; charset=\"us-ascii\"" >> /log-location/$datetime.mail
echo "" >> /log-location/$datetime.mail

cat /log-location/$datetime.mail /log-location/$datetime.fail /log-location/$datetime.log > /log-location/$datetime.html

#now clean up the file with unwanted text
sed '/^$/d' /log-location/$datetime.html > /log-location/$datetime.htm
#wait a sec to get file settled.
sleep 1
# ----------------------------------- end of preparing the email -------------------------------------------

# let's send the email out......

/usr/sbin/sendmail helpdesk@your.organistation.com < /log-location/$datetime.htm

sleep 1
rm /log-location/$datetime.mail 
rm /log-location/$datetime.fail 
rm /log-location/$datetime.log 
rm /log-location/$datetime.html
rm /log-location/$datetime.htm

exit 0
# 
